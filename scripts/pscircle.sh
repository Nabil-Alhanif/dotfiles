#!/bin/bash

set -e

output=/tmp/pscircle.png

pscircle \
    --output=$output \
    --output-width=1920 \
    --output-height=1080 \
    --max-children=75
    --tree-radius-increment=100 \
    --dot-radius=4 \
    --link-width=1.6 \
    --tree-font-size=13 \
    --toplists-font-size=14 \
    --toplists-bar-width=40 \
    --toplists-row-height=21 \
    --toplists-bar-height=4 \
    --tree-center=-100.0:0.0 \
    --cpulist-center=500.0:-80.0 \
    --memlist-center=500.0:80.0

if command -v feh >/dev/null; then
    feh --bg-fill $output
    rm $output
fi
