# !/bin/bash

# Terminate already running udiskie instances
killall -q udiskie

# Wait until the processes have been shut down
while pgrep -u SUID -x udiskie > /dev/null; do sleep 1; done

# Launh udiskie
udiskie --tray &

echo "Udiskie launched..."
