# !/bin/bash

if pgrep polybar; then
    pkill polybar
    exec polybar mybar &
else
    exec polybar mybar &
fi
exit
