local dirname = debug.getinfo(1).source:match("@?(.*/)")
package.path = package.path .. ';' .. dirname .. '?.lua;'

local helper = require('lua.modules.helper')

helper.hc('emit_hook reload')

os.execute('xsetroot -solid "#5A8E3A"')

-- Remove all existing keybindings
helper.hc('keyunbind --all')
helper.hc('mouseunbind --all')
helper.hc('unrule -F')

-- Load configuration
require('lua.configurations.init')

helper.hc('unlock')

-- Auto start process
require('lua.modules.auto_start')
