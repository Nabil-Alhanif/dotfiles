local helper = require('lua.modules.helper')

local keybindings   = require('lua.configurations.bindings.keys')
local mousebindings = require('lua.configurations.bindings.mouse')
local attributes    = require('lua.configurations.attributes')
local settings      = require('lua.configurations.settings')
local rules         = require('lua.configurations.rules')

helper.do_config('keybind ',   keybindings)
helper.do_config('mousebind ', mousebindings)
helper.do_config('attr ',      attributes)
helper.do_config('set ',       settings)
helper.do_config('rule ',      rules)
