--      ██╗  ██╗███████╗██╗   ██╗███████╗
--      ██║ ██╔╝██╔════╝╚██╗ ██╔╝██╔════╝
--      █████╔╝ █████╗   ╚████╔╝ ███████╗
--      ██╔═██╗ ██╔══╝    ╚██╔╝  ╚════██║
--      ██║  ██╗███████╗   ██║   ███████║
--      ╚═╝  ╚═╝╚══════╝   ╚═╝   ╚══════╝

local gtable = require('lua.modules.gtable')
local helper = require('lua.modules.helper')


-- ============
-- | Variable |
-- ============

local Mod     = 'Mod4-'
local Alt     = 'Mod1-'
local Shift   = 'Shift-'
local Control = 'Control-'
local Left    = 'h'
local Down    = 'j'
local Up      = 'k'
local Right   = 'l'

local resizestep = '0.02'


-- ===============
-- | Keybindings |
-- ===============

local keybinding = gtable.join({
    -- ===========
    -- | Session |
    -- ===========

    [Mod..Shift..'e'] = 'quit',
    [Mod..Shift..'r'] = 'reload',
    [Mod..Shift..'q'] = 'close',


    -- =================
    -- | Spawn Process |
    -- =================

    -- Spawn $TERMINAL
    [Mod..'Return']  = 'spawn ${TERMINAL:-kitty}',
    -- Spawn Rofi
    [Mod..'d']       = 'spawn rofi -mod drun,run -show drun',


    -- ============
    -- | Movement |
    -- ============

    -- ===================
    -- | Focusing Client |
    -- ===================

    [Mod..'Left']  = 'focus left',
    [Mod..'Down']  = 'focus down',
    [Mod..'Up']    = 'focus up',
    [Mod..'Right'] = 'focus right',
    [Mod..Left]    = 'focus left',
    [Mod..Down]    = 'focus down',
    [Mod..Up]      = 'focus up',
    [Mod..Right]   = 'focus right',

    -- =================
    -- | Moving Client |
    -- =================

    [Mod..Shift..'Left']  = 'shift left',
    [Mod..Shift..'Down']  = 'shift down',
    [Mod..Shift..'Up']    = 'shift up',
    [Mod..Shift..'Right'] = 'shift right',
    [Mod..Shift..Left]    = 'shift left',
    [Mod..Shift..Down]    = 'shift down',
    [Mod..Shift..Up]      = 'shift up',
    [Mod..Shift..Right]   = 'shift right',


    -- ====================
    -- | Splitting Frames |
    -- ====================

    -- Create and empty frame at the specified direction
    [Mod..'u'] = 'split bottom 0.5',
    [Mod..'o'] = 'split right 0.5',
    -- Let the current frame explode into suframes
    [Mod..Control..'Space'] = 'split explode',


    -- ===================
    -- | Resizing Frames |
    -- ===================

    [Mod..Control..'Left']  = 'resize left +'..resizestep,
    [Mod..Control..'Down']  = 'resize down +'..resizestep,
    [Mod..Control..'Up']    = 'resize up +'..resizestep,
    [Mod..Control..'Right'] = 'resize right +'..resizestep,
    [Mod..Control..Left]    = 'resize left +'..resizestep,
    [Mod..Control..Down]    = 'resize down +'..resizestep,
    [Mod..Control..Up]      = 'resize up +'..resizestep,
    [Mod..Control..Right]   = 'resize right +'..resizestep,


    -- =============
    -- | Layouting |
    -- =============

    [Mod..'r']          = 'remove',
    [Mod..'s']          = 'floating toggle',
    [Mod..'f']          = 'fullscreen toggle',
    [Mod..Shift..'f']   = 'set_attr clients.focus.floating toggle',
    [Mod..Shift..'m']   = 'set_attr clients.focus.minimized true',
    [Mod..Control..'m'] = 'jumpto last-minimized',
    [Mod..'p']          = 'pseudotile toggle',


    -- =========
    -- | Focus |
    -- =========

    [Mod..'BackSpace']    = 'cycle_monitor',
    [Mod..'Tab']          = 'cycle_all +1',
    [Mod..'Shift'..'Tab'] = 'cycle_all -1',
    [Mod..'c']            = 'cycle',
    [Mod..'i']            ='jumpto urgent',
})


-- ================
-- | More Layouts | But I Don't Understand
-- ================

-- The following cycles through the available layouts within a frame, but skips
-- layouts, if the layout change wouldn't affect the actual window positions.
-- I.e. if there are two windows within a frame,the gtid layout is skipped.
helper.hc('keybind '..Mod..'Space '
..'or , and . compare tags.focus.curframe_wcount = 2 '
..'. cycle_layout +1 vertical horizontal max vertical grid '
..', cycle_layout +1')


-- ========
-- | Tags |
-- ========

local tag_names = {}
local tag_keys  = {}

for i = 1,9 do tag_names[i-1]=i end
for i = 1,9 do tag_keys[i-1]=i  end
tag_keys[9] = 0


-- Cycle through tags
keybinding = gtable.join(keybinding, {
    [Mod..'.'] = 'use_index +1 --skip-visible',
    [Mod..','] = 'use_index -1 --skip-visible',
})

for index, value in pairs(tag_names) do
    helper.hc('add "'..value..'"')

    local key = tag_keys[index]
    if (not(key == nil or key == '')) then
        keybinding = gtable.join(keybinding, {
            [Mod..key] = 'use_index "'..index..'"',
            [Mod..Shift..key] = 'move_index "'..index..'"',
        })
    end
end


return keybinding
