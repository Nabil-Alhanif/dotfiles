local gtable = require('lua.modules.gtable')
local helper = require('lua.modules.helper')


-- ============
-- | Variable |
-- ============

local Mod = 'Mod4-'


-- ==================
-- | Mouse Bindings |
-- ==================
local mousebinding = gtable.join({
    [Mod..'Button1'] = 'move',
    [Mod..'Button2'] = 'zoom',
    [Mod..'Button3'] = 'resize',
})

return mousebinding
