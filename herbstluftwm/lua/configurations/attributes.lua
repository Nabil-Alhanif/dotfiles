local gtable = require('lua.modules.gtable')
local helper = require('lua.modules.helper')

-- ==============
-- | Attributes |
-- ==============

local attributes = gtable.join({
    -- =========
    -- | Theme |
    -- =========

    ['theme.tiling.reset']   = '1',
    ['theme.floating.reset'] = '1',

    ['theme.title_height'] = '15',
    ['theme.title_font']   = 'Dejavu Sans:pixelsize=12',
    ['theme.padding_top']  = '2',
    ['theme.active.color'] = '#345F0Cef',
    ['theme.title_color']  = '#ffffff',
    ['theme.normal.color'] = '#323232dd',
    ['theme.urgent.color'] = '#7811A1dd',
    ['theme.normal.title_color'] = '#898989',
    ['theme.inner_width']  = '1',
    ['theme.inner_color']  = 'black',
    ['theme.border_width'] = '3',
    ['theme.floating.border_width'] = '4',
    ['theme.floating.outer_width']  = '1',
    ['theme.floating.outer_color']  = 'black',
    ['theme.active.inner_color']    = '#789161',
    ['theme.urgent.inner_color']    = '#9A65B0',
    ['theme.normal.inner_color']    = '#606060',
    ['theme.active.outer_color']    = '#789161',
    ['theme.urgent.outer_color']    = '#9A65B0',
    ['theme.normal.outer_color']    = '#606060',
    ['theme.active.outer_width']    = '1',
    ['theme.background_color']      = '#141414',
})

return attributes
