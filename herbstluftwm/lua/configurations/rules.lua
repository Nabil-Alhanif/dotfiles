local gtable = require('lua.modules.gtable')
local helper = require('lua.modules.helper')

-- ========
-- | Rule |
-- ========

local rules = gtable.join({
    ['focus=on'] = '',
    ['floatplacement=smart'] = '',
    ['windowtype~"_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)"'] = 'floating=on',
    ['windowtype="_NET_WM_WINDOW_TYPE_DIALOG"'] =  'focus=on',
    ['windowtype~"_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)"'] = 'manage=off',
})

return rules
