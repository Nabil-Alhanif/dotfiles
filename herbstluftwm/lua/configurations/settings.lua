local gtable = require('lua.modules.gtable')
local helper = require('lua.modules.helper')

-- =========
-- | Settings |
-- =========

local settings = gtable.join({
    -- =========
    -- | Theme |
    -- =========

    ['frame_border_active_color'] = '#222222cc',
    ['frame_border_normal_color'] = '#101010cc',
    ['frame_bg_normal_color']     = '#565656aa',
    ['frame_bg_active_color']     = '#345F0Caa',
    ['frame_border_width']        = '1',
    ['always_show_frame']         = 'on',
    ['frame_bg_transparent']      = 'on',
    ['frame_transparent_width']   = '5',
    ['frame_gap']                 = '4',

    ['window_gap']                = '0',
    ['frame_padding']             = '0',
    ['smart_window_surroundings'] = 'off',
    ['smart_frame_surroundings']  = 'on',
    ['mouse_recenter_gap']        = '0',
    ['tree_style']                = '╾│ ├└╼─┐',
})

return settings
