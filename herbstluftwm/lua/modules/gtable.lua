-- Table modules
-- Copied from AwesomwWM gears.table with some modification

local rtable = table

local gtable = {}

-- Join all tables given as arguments
-- This will iterate over all tables and insert their entries into a new table
function gtable.join(...)
    local ret = {}
    print("\nGTABLE\n")
    for i = 1, select('#', ...) do
        local t = select(i, ...)
        if t then
            for k, v in pairs(t) do
                if type(k) == "number" then
                    rtable.insert(ret, v)
                else
                    ret[k] = v
                end
                print(k, v)
            end
        end
    end
    return ret
end

-- Override elements in the first table by the one in the second.
-- Note that this method doesn't copy entries found in '__index'
function gtable.crush(t, set, raw)
    if raw then
        for k, v in pairs(set) do
            rawset(t, k, v)
        end
    else
        for k, v in pairs(set) do
            t[k] = v
        end
    end
    return t;
end

return gtable
