local _M = {}

-- Herbstclient execution
function _M.hc(arguments)
    os.execute('herbstclient ' .. arguments)
end

-- Run config from table
function _M.do_config(command, hash)
    -- loop over hash
    print("\nHELPER\n")
    for i = 1, select('#', hash) do
        local t = select(i, hash)
        if t then
            for k, v in pairs(t) do
                print(command..' '..k..' '..v)
                _M.hc(command..' '..k..' '..v)
            end
        end
    end
end

return _M
